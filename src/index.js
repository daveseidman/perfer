import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';
import onChange from 'on-change';
import './index.scss';

const symbols = ['&nbsp;', '⬤', '◯', '■', '□', '│', '─', '┌', '┐', '└', '┘', '┬', '┴', '├', '┤', '┼'];
const expirationDate = new Date();
const duration = 365; // days
expirationDate.setTime(expirationDate.getTime() + (duration * 24 * 60 * 60 * 1000));

class App {
  constructor() {
    autoBind(this);
    const state = {
      cols: 10,
      rows: 20,
      values: null,
    };

    this.el = createEl('div', { className: 'app' });

    this.grid = createEl('div', { className: 'grid' });


    const savedState = document.cookie.match(new RegExp('(^| )perferState=([^;]+)'));
    if (savedState) {
      const newState = JSON.parse(savedState[2]);
      state.cols = newState.cols;
      state.rows = newState.rows;
      state.values = newState.values;
    } else {
      state.values = Array(state.cols).fill(0).map(x => Array(state.rows).fill(0));
    }
    this.state = onChange(state, this.update, { pathAsArray: true, ignoreKeys: [] });

    this.controls = createEl('div', { className: 'controls' });
    this.colsSelector = createEl('input', { type: 'text', placeholder: 'cols', value: this.state.cols }, {}, { change: ({ target }) => { this.state.cols = parseInt(target.value); } });
    this.rowsSelector = createEl('input', { type: 'text', placeholder: 'rows', value: this.state.rows }, {}, { change: ({ target }) => { this.state.rows = parseInt(target.value); } });

    addEl(this.controls, this.colsSelector, this.rowsSelector);

    addEl(this.el, this.controls, this.grid);


    this.drawGrid();
  }

  update(path, current, previous) {
    console.log(`${path}: ${previous} -> ${current}`);
    if (path[0] === 'cols' || path[0] === 'rows') {
      // TODO: resize the grid without changing values;
      // this.state.values = Array(this.state.cols).fill(0).map(x => Array(this.state.rows).fill(0));
      // this.state.values = Object.assign(this.state.values, )

      // this.state.cols = cols
    }
    if (path[0] === 'values') this.drawGrid();
  }

  drawGrid() {
    this.grid.innerHTML = '';

    for (let y = 0; y < this.state.rows; y += 1) {
      for (let x = 0; x < this.state.cols; x += 1) {
        const cell = createEl('button', { className: 'grid-cell', innerHTML: symbols[this.state.values[x][y]] }, {}, { click: (e) => {
          this.state.values[x][y] = e.metaKey ? 0 : this.state.values[x][y] + (e.altKey ? -1 : 1);
          if (this.state.values[x][y] >= symbols.length) this.state.values[x][y] = 0;
          if (this.state.values[x][y] < 0) this.state.values[x][y] = symbols.length - 1;
          // console.log(x, y);
          cell.innerHTML = symbols[this.state.values[x][y]];
        } });
        addEl(this.grid, cell);
      }
      addEl(this.grid, createEl('br'));
    }
    document.cookie = `perferState=${JSON.stringify(this.state)};expires=${expirationDate.toUTCString()};path=/`;
  }
}


const app = new App();
window.app = app;
addEl(app.el);
